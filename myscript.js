
function addArc(i){
	var fastaurl = (String($("#alignment_" + i).children()[1].getElementsByTagName('a')[0]) + ".fasta")
	var leourl = "http://leonidas.utk.edu/cgi-bin/parc.py?l=" + fastaurl;
	var iframerow = '<iframe src="' + leourl + '" width=100% height=40px scrolling="no" frameBorder="0"><p>Your browser does not support iframes.</p></iframe>';
	$("#alignment_" + i).children().eq(2).append( iframerow );
	}

	
function ncbi(index){
	var gi = $("tr.dflLnk").eq(index).children().eq(7).children()[0].href.split('protein/')[1].split('?')[0];
	//console.log(gi);
	var leourl = "http://leonidas.utk.edu/cgi-bin/parc.py?ncbi_id=" + gi;
	var iframerow = '<div><iframe src="' + leourl + '" width=100% height=40px scrolling="no" frameBorder="0"><p>Your browser does not support iframes.</p></iframe></div>';
	$("tr.dflLnk").eq(index).children().eq(1).append( iframerow );
	}
	
	
function rock(){	
	var host = window.location.host;

	if (host == 'www.ebi.ac.uk'){
		var count = 0;
		$(".belowThreshold").each(function(index){addArc(count + index);})
		count += $(".belowThreshold").length;
		$(".aboveThreshold").each(function(index){addArc(count + index);})	
		}
	else if (host == 'blast.ncbi.nlm.nih.gov' || host == "www.ncbi.nlm.nih.gov"){
		$("tr.dflLnk").each(function(index){ncbi(index);})
		}
}

rock();